#!/usr/bin/env python3
"""
Alternate between a command and sleeping, indefinitely (or for a fixed duration.)

Usage:
   periods.py tidle <t_idle> tcmd <t_cmd> [-d=<duration> -o=<stdout> -e=<stderr>] [ -g=<dbglvl> | -q ] [--] <command_line>...

Options:
  -h --help                   Show this screen.
  --version                   Show version.
  -d --duration=<duration>    Total time before exit [default: 0]
  -e --stderr=<stderr>        Err file [default: /dev/null]
  -o --stdout=<stdout>        Log file [default: /dev/null]
  -g --debug-level=<dgblvl>   Debug level (D<W<E<C<N) [default: E]
  -q --quiet                  Quiet

Examples:
    periods tidle 2 tcmd 2 -e t.e -o t.o -- sleep 3
"""

import signal
import os
import sys
import logging
import subprocess
import time
import datetime
import math
import atexit

from docopt import docopt

PROCESS = None

class TimeMeasure:
    def __init__(self):
        self.reset()

    def reset(self):
        self.monotonic = time.monotonic()
        self.current = datetime.datetime.now()
        self.total = datetime.timedelta()
        pass

    def take(self):
        now = datetime.datetime.now()
        elapsed = now - self.current
        self.current = now
        return elapsed

    def cum(self):
        elapsed = self.take()
        self.total = self.total + elapsed
        return elapsed

    def get_total_ms(self):
        return self.total.seconds + (self.total.microseconds * 1e-6)

    def get_delta_total(self):
        return self.total


def cleanup():
    global PROCESS
    print("cleaning")
    if PROCESS is not None:
        print(PROCESS)
        print("cleaning a process")
        # PROCESS.terminate()
        os.killpg(os.getpgid(PROCESS.pid), signal.SIGTERM)

def run_command(command):
    # We record the process to a global var so we can kill it on the atexit
    global PROCESS

    PROCESS = subprocess.Popen(command, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    (stdout, stderr) = PROCESS.communicate()
    if PROCESS.returncode == 124:
        logging.warning("command reached timeout")

    PROCESS = None

    return (stdout, stderr)

def str_to_debug(str):
    if str == "D":
        return logging.DEBUG
    if str == "W":
        return logging.WARNING
    if str == "E":
        return logging.ERROR
    if str == "C":
        return logging.CRITICAL
    if str == "Q":
        return logging.NOSET

    return logging.CRITICAL

def deltatime_to_sec(d):
    return d.seconds + (d.microseconds * 1e-6)

def play_period(command, idle_time, command_time, t0, outfd = None, errfd = None):
    period_time = (sleep_period + command_period)

    (stdout, stderr) = run_command(command)

    if outfd is not None:
        outfd.write(stdout.decode("utf-8"))
        outfd.flush()

    if errfd is not None:
        errfd.write(stderr.decode("utf-8"))
        errfd.flush()

    now = datetime.datetime.now()
    sleep_for = period_time - (now - t0) % period_time

    seconds_to_complete = deltatime_to_sec(sleep_for)
    logging.info("time to complete %s " % seconds_to_complete)

    time.sleep(seconds_to_complete)
    print(datetime.datetime.now())

timedelta_nul = datetime.timedelta()

if __name__ == '__main__':

    # Docopt arguments
    arguments = docopt(__doc__, version='sgwrap launcher 0.0')
    if arguments["--quiet"]:
        dbg_level = "N"
    else:
        dbg_level =  arguments["--debug-level"]

    atexit.register(cleanup)

    logging.basicConfig(level=str_to_debug(dbg_level))

    # Time to be idle
    sleep_period = datetime.timedelta(seconds = int(arguments["<t_idle>"]))
    # Time that the program should pass
    command_period = datetime.timedelta(seconds = int(arguments["<t_cmd>"]))

    command = arguments["<command_line>"]

    # add one second to let the program close corretly (is it a good idea)
    command = [ "timeout", str(command_period.seconds + 0.5)] + command

    period_time = (sleep_period + command_period)
    logging.info("one period is %s" % period_time)

    total_time = TimeMeasure()
    duration =  datetime.timedelta(seconds = int(arguments["--duration"]))

    nb_iteration = int(math.ceil(duration / period_time))

    t0 = datetime.datetime.now()

    with open(arguments["--stdout"], "w") as outfd, open(arguments["--stderr"], "w") as errfd:

        if duration == timedelta_nul:
            logging.info("Infinite loop")
            while True:
                play_period(command, sleep_period, command_period, t0,
                    errfd = errfd, outfd = outfd)
                total_time.cum()
        else:
            logging.info("playing %s iterations" % nb_iteration)
            for _i in range(nb_iteration):
                logging.info("iteration : %s/%s" % (_i, nb_iteration))
                play_period(command, sleep_period, command_period, t0,
                    errfd = errfd, outfd = outfd)
                total_time.cum()
