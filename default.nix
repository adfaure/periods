{
  pkgs ? import (
    fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.09.tar.gz") {},
}:

let
  pythonPackages = pkgs.python37Packages;
  python = pkgs.python37;

  self = rec {
    periods = pythonPackages.buildPythonPackage rec {
      pname = "periods";
      version = "local";
      propagatedBuildInputs = with pythonPackages; [
        docopt
        pkgs.ps
        pkgs.procps
      ];
      src = pkgs.lib.sourceByRegex ./. [
        "periods.py"
        "periods"
        "setup.py"
      ];
    };

    dev = pkgs.mkShell rec {
      name = "t";
      buildInputs = [periods pkgs.ps];
    };

  };
in
  self
