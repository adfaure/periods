#!/usr/bin/env python
from distutils.core import setup

setup(name='periods',
    version='0.1.0',
    py_modules=['periods'],
    scripts=['periods'],

    python_requires='>=3.6',
    install_requires=['docopt>=0.6.2'],

    description='periodicaly alternate between a command and idle time',
    author='Adrien Faure',
    author_email='adrien.faure@protonmail.com',
    url='https://github.com/adfaure/periods',
    license='MIT',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],
)
